from admin_tools.dashboard.dashboards import *

from app.torrents.modules import *
from app.trackers.modules import *
from app.common.modules import *

class DefaultIndexDashboard(Dashboard):
    """
    The default dashboard displayed on the admin index page.
    """
    def init_with_context(self, context):
        """
        Initializer.
        """
        site_name = get_admin_site_name(context)

        # append an app list module for "Administration"
        self.children.append(modules.AppList(
            'Administration', models=('django.contrib.*',),
        ))

        # append a recent actions module
        self.children.append(modules.RecentActions(
            'Recent Actions', 6
        ))

        # append a feed module
        self.children.append(modules.Feed(
            'Latest Django News', feed_url='http://www.djangoproject.com/rss/weblog/', limit=6
        ))

        # append an app list module for "Applications"
        self.children.append(TorrentStatistics())
        self.children.append(TrackerStatistics())
        self.children.append(GroupStatistics())
        self.children.append(QuantityStatistics())
        self.children.append(ServiceStatus())