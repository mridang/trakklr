from django.contrib.auth.models import *
from django.contrib import admin

from overseer.models import *
from overseer.admin import *

admin.autodiscover()

admin.site.unregister(User)

admin.site.unregister(Service)

class ModdedServiceAdmin(ServiceAdmin):
    """
    Modified service administration with icon support
    """
    list_display = ('name', 'state', 'order', 'date_updated')

    def state(self, obj):
        """
        Returns an icon depending upon the status
        """
        if obj.status == 0:
            return '<img alt="Working" style="height: 11.5px;" src="/media/img/admin/icon_success.gif">'
        if obj.status == 1:
            return '<img alt="Errors" style="height: 11.5px;" src="/media/img/admin/icon_alert.gif">'
        if obj.status == 2:
            return '<img alt="Unavailable" style="height: 11.5px;"  src="/media/img/admin/icon_error.gif">'
    state.allow_tags=True

admin.site.register(Service, ModdedServiceAdmin)

admin.site.unregister(Event)

class ModdedEventAdmin(EventAdmin):
    """
    Modified event administration with icon support
    """
    list_display = ('date_created', 'description', 'state', 'date_updated')

    def state(self, obj):
        """
        Returns an icon depending upon the status
        """
        if obj.status == 0:
            return '<img alt="Working" style="height: 11.5px;" src="/media/img/admin/icon_success.gif">'
        if obj.status == 1:
            return '<img alt="Errors" style="height: 11.5px;" src="/media/img/admin/icon_alert.gif">'
        if obj.status == 2:
            return '<img alt="Unavailable" style="height: 11.5px;"  src="/media/img/admin/icon_error.gif">'
    state.allow_tags=True

admin.site.register(Event, ModdedEventAdmin)

admin.site.unregister(EventUpdate)

class ModdedEventUpdateAdmin(EventUpdateAdmin):
    """
    Modified event-update administration with icon support
    """
    list_display = ('date_created', 'message', 'state', 'event')

    def state(self, obj):
        """
        Returns an icon depending upon the status
        """
        if obj.status == 0:
            return '<img alt="Working" style="height: 11.5px;" src="/media/img/admin/icon_success.gif">'
        if obj.status == 1:
            return '<img alt="Errors" style="height: 11.5px;" src="/media/img/admin/icon_alert.gif">'
        if obj.status == 2:
            return '<img alt="Unavailable" style="height: 11.5px;"  src="/media/img/admin/icon_error.gif">'
    state.allow_tags=True

admin.site.register(EventUpdate, ModdedEventUpdateAdmin)

admin.site.unregister(Group)
