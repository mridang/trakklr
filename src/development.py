import os
import sys

DEBUG = True

# A boolean that turns on/off template debug mode. If this is True, the fancy
# error page will display a detailed report for any template/request exception.
TEMPLATE_DEBUG = DEBUG

# Path to the current project directory. Django uses this path to build relative
# paths.
PROJECT_PATH = os.path.realpath(os.path.dirname(__file__))

# A tuple that lists people who get code error notifications. When DEBUG=False
# and a view raises an exception, Django will email these people with error details.
ADMINS = (('Mridang Agarwalla', 'mridang.agarwalla@gmail.com'),)

# A tuple in the same format as ADMINS that specifies who should get broken-link
# notifications when SEND_BROKEN_LINK_EMAILS=True.
MANAGERS = ADMINS

# A dictionary containing the settings for all databases to be used with Django.
# It is a nested dictionary of options.
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(PROJECT_PATH, 'sqlitedb'),
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': ''
    },
    'logging': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(PROJECT_PATH, 'sqlitedb'),
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': ''
    }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
TIME_ZONE = 'Europe/Helsinki'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = os.path.join(PROJECT_PATH, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component.
MEDIA_URL = '/media/'

# Absolute path to the directory that holds static files.
# Example: "/home/static/media.lawrence.com/"
STATIC_ROOT = os.path.join(PROJECT_PATH, 'static')

# URL that handles the static content served from STATIC_ROOT. Make sure to use a
# trailing slash if there is a path component.
STATIC_URL = '/static/'

# Absolute path to the directory that holds compressed files.
# Example: "/home/compressed/media.lawrence.com/"
COMPRESS_ROOT = os.path.join(PROJECT_PATH, 'static')

# URL that handles the media served from COMPRESS_ROOT. Make sure to use a
# trailing slash if there is a path component.
COMPRESS_URL = '/static/'

# Absolute path to the directory that holds temporary stuff.
# Example: "/home/media/media.lawrence.com/"
TEMP_ROOT = os.path.join(PROJECT_PATH, 'temporary')

# URL that handles the media served from TEMP_ROOT. Make sure to use a
# trailing slash if there is a path component.
TEMP_URL = '/temporary/'

# URL prefix for admin media -- CSS, JavaScript and images.
# trailing slash if there is a path component.
ADMIN_MEDIA_PREFIX = '/media/'

# Make this unique, and don't share it with anybody. This is the unique key that
# is often used for encrypting data.
SECRET_KEY = 'i%uc&q734uzr+)x*i@&fs1n!i_+6nv9a&wikpon(%0aj#nnt^l'

# List of callables that know how to import templates from various sources.
#
TEMPLATE_LOADERS = (
  'django.template.loaders.filesystem.Loader',
  'django.template.loaders.app_directories.Loader'
)

# A tuple of middleware classes to use. The middlewares are used in the order
# that are stacked so the order is extremely important.
MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'peavy.middleware.RequestLoggingMiddleware',
    'django.middleware.gzip.GZipMiddleware',
    'slimmer.middleware.CompressHtmlMiddleware',
)

# A string representing the full Python import path to your root URLconf. For
# example: "mydjangoapps.urls".
ROOT_URLCONF = 'urls'

# List of locations of the template source files searched by
# django.template.loaders.filesystem.Loader, in search order.
TEMPLATE_DIRS = (
     os.path.join(PROJECT_PATH, 'templates')
)

# A tuple of strings designating all applications that are enabled in this Django
# installation. Each string should be a full Python path to a Python Django package.
INSTALLED_APPS = (
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.admin',
    'django.contrib.auth',
    'overseer',
    'erroneous',
    'dbbackup',
    'south',
    'peavy',
    'compressor',
    'chronograph',
    'djcelery',
    'constance',
    'app.trackers',
    'app.common',
    'app.torrents',
)

# The backend to use for sending emails. SMTP is the most common backend for
# sending and receiving email.
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# The host to use for sending email. This is the used by the SMTP backend i.e.
# django.core.mail.backends.smtp.EmailBackend
EMAIL_HOST='localhost'

# The port to use for sending email. This is the used by the SMTP backend i.e.
# django.core.mail.backends.smtp.EmailBackend
EMAIL_PORT=1025

# If SESSION_SAVE_EVERY_REQUEST  is True, Django will save the session to the
# database on every single request.
SESSION_SAVE_EVERY_REQUEST = True

#A bunch of default contest processortrs that are used by the templating engine
#I've added these separately as they were missing.
TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.request",
)

# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
    },
    'filters': {
        'basic': {
            '()': 'peavy.filters.BasicFilter',
        },
        'meta': {
            '()': 'peavy.filters.MetaFilter',
        }
    },
    'handlers': {
        'null': {
            'level':'DEBUG',
            'class':'django.utils.log.NullHandler',
        },
        'console':{
            'level':'DEBUG',
            'class':'logging.StreamHandler',
            'formatter': 'standard'
        },
        'peavy': {
            'level': 'INFO',
            'class': 'peavy.handlers.DjangoDBHandler',
            'filters': ['basic', 'meta'],
            'formatter': 'standard'
        }
    },
    'loggers': {
        'django': {
            'handlers':['console'],
            'propagate': True,
            'level':'INFO',
        },
        'django.db.backends': {
            'handlers': ['console'],
            'level': 'ERROR',
            'propagate': False,
        },
        'scraper': {
            'handlers': ['peavy', 'console'],
            'level': 'DEBUG',
        },
        'checker': {
            'handlers': ['peavy', 'console'],
            'level': 'DEBUG',
        },
        'querier': {
            'handlers': ['peavy', 'console'],
            'level': 'DEBUG',
        },
    }
}

# A dictionary containing the settings for all caches to be used with Django. It
# is a nested dictionary whose contents maps cache aliases to a dict of cache options.
CACHES = {'default': {'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',}}

# The name of the databse to which Peavy should log to. The list of databases are
# defined in the DATABASES setting.
PEAVY_DATABASE_NAME = 'default'

# The fully qualified name of the databse router that Peavy should use. To log
# to the default databse use the Django router.
DATABASE_ROUTERS = ['peavy.routers.DjangoDBRouter']

# Boolean that decides if compression will happen. To test compression when DEBUG
# is True COMPRESS_ENABLED must also be set to True.
COMPRESS_ENABLED = True

# This is a list of filters that will be applied to CSS. We will try and use
# a simple python based engine.
COMPRESS_CSS_FILTERS = ['compressor.filters.cssmin.CSSMinFilter']

#This is a list of filters that will be applied to JS. We will try and use
# a simple python based engine.
COMPRESS_JS_FILTERS = ['compressor.filters.jsmin.JSMinFilter']

# The backend to use when parsing the JavaScript or Stylesheet files. LxmlParser
# is the fastest available parser so we'll use that
COMPRESS_PARSER = 'compressor.parser.LxmlParser'

# The backend to use when backing up the databse SQL files using the database's
# SQL dump tool
DBBACKUP_STORAGE = 'dbbackup.storage.filesystem_storage'

# The location of the directory where the backed up SQL files should be stored
# from the backup tool
DBBACKUP_FILESYSTEM_DIRECTORY = os.path.join(PROJECT_PATH, 'backups')

# This is the dorectory where the torrent files are cached. This should be and
# absolute path.
TORRENT_DIR = os.path.join(PROJECT_PATH, 'cache', 'torrents')

# Controls the directory inside COMPRESS_ROOT that compressed files will be
# written to.
COMPRESS_OUTPUT_DIR = 'compressed'

# This is the configuration for the main administration dashbaord that shows up
# in the admin panel.
ADMIN_TOOLS_INDEX_DASHBOARD = 'admin.dashboard.DefaultIndexDashboard'

# This is the URL configuration AQMP server installed locally. We have to pass
# the crendentials, the port and the path.
BROKER_URL = "amqp://trakklr:trakklr@localhost:5672//trakklr"

# This is the number of worker processes that will be working on the default
# celery queue.
CELERYD_CONCURRENCY = 10

# This is the list of files that celery should import. We should import all the
# check.py files.
CELERY_IMPORTS = ('app.torrents.checks', 'app.common.checks', 'app.trackers.checks', 'app.torrents.tasks', 'app.trackers.tasks', 'app.common.tasks')

# Task hard time limit in seconds. The worker processing the task will be
# killed and replaced with a new one when this is exceeded.
CELERYD_TASK_TIME_LIMIT = 300

# This is the URL of the torrent cache from whcih torrents will be fetched in
# the event that a torrent file is missing.
TORRENT_URL = 'http://torcache.com/torrent/%s.torrent'
