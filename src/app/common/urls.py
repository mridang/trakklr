from django.conf.urls.defaults import *
from django.contrib import admin
from django.conf import settings

urlpatterns = patterns('',
    url(r'^privacy/$', 'django.views.generic.simple.direct_to_template', {'template' : 'common/privacy.htm'}, 'privacy'),
    url(r'^terms/$', 'django.views.generic.simple.direct_to_template', {'template' : 'common/terms.htm'}, 'terms'),
    url(r'^about/$', 'django.views.generic.simple.direct_to_template', {'template' : 'common/about.htm'}, 'about'),
)