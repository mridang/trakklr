from datetime import *

from admin_tools.dashboard import modules
from overseer.models import *

from app.trackers.models import *
from app.torrents.models import *

class GroupStatistics(modules.DashboardModule):
    """
    Dashboard module with groups statistics.
    """
    title = "Types"
    template = 'common/dashboards/group_stats.html'

    def is_empty(self):
        """
        Empty Checker
        """
        return False

    def __init__(self, *args, **kwargs):
        """
        Initializer.
        """
        super(GroupStatistics, self).__init__(*args, **kwargs)

        self.trackers = Tracker.objects.grouped_by_type()
        self.torrents = Torrent.objects.grouped_by_type()


class QuantityStatistics(modules.DashboardModule):
    """
    Dashboard module with quantity statistics.
    """
    title = "Tracker Statistics"
    template = 'common/dashboards/quantity_stats.html'

    def is_empty(self):
        """
        Empty Checker
        """
        return False

    def __init__(self, *args, **kwargs):
        """
        Initializer.
        """
        super(QuantityStatistics, self).__init__(*args, **kwargs)

        self.trackers = Tracker.objects.count()
        self.torrents = Torrent.objects.count()


class ServiceStatus(modules.DashboardModule):
    """
    Dashboard module with service status.
    """
    title = "Service Status"
    template = 'common/dashboards/service_status.html'

    def is_empty(self):
        """
        Empty Checker
        """
        return False

    def __init__(self, *args, **kwargs):
        """
        Initializer.
        """
        super(ServiceStatus, self).__init__(*args, **kwargs)

        self.services = Service.objects.all()
        self.events = list(Event.objects.filter(date_updated=datetime.datetime.today())[0:6])