import urlparse
import json
import socket

import requests
from lxml import *

from django.conf import settings
from custom.functions import *

class WebsiteInformation():
    """
    Common backend class for backend functions.
    """

    @staticmethod
    def process(url) :
        """
        Gets the feed path and title for a given address.
        """
        try:

            response = requests.get(url % 0)

            document = html.fromstring(response.text)
            document.make_links_absolute(url % 0)

            enabled = True
            feed = document.xpath("//head/link[@rel='alternate']")[0].get('href') if document.xpath("//head/link[@rel='alternate']") else None
            title = document.xpath('//title/text()')[0]

            return (title, feed, enabled)

        except Exception, e:
            raise e
