from django.contrib import admin
from django.db.models import Max

from app.common.models import *

class LinkInline(admin.TabularInline):
    """
    Administration interface for the links.
    """
    list_display = ('date', 'name', 'url')
    ordering = ['-date']
    list_per_page = 100
    search_fields = ('date',)
    date_hierarchy = 'date'
    list_display_links = ['name']
    can_delete = False
    readonly_fields = ('date', 'name', 'url')
    exclude = ('seeders', 'leechers', 'torrent')
    model = Link
    max_num = 0
    list_per_page = 100

    def queryset(self, request):
        """
        Returns only the latest records
        """
        qs = super(LinkInline, self).queryset(request)
        return qs.filter(date__gte=Link.objects.all().aggregate(Max('date'))['date__max'].date())

    def has_add_permission(self, request, obj=None):
        """
        Disable adding.
        """
        return False

    def has_change_permission(self, request, obj=None):
        """
        Disable editing.
        """
        return False

    def has_delete_permission(self, request, obj=None):
        """
        Disable deleting.
        """
        return False


class SiteAdmin(admin.ModelAdmin):
    """
    Administration interface for the sites.
    """
    list_display = ('title', 'host', 'is_accessible', 'rows_listed', 'days_since', 'latest_torrents')
    ordering = ['-id']
    list_per_page = 100
    search_fields = ['title', 'host']
    list_display_links = ['title']
    #list_filter = ('host',)
    readonly_fields = []
    exclude = None
    inlines = [LinkInline]
    max_num = 0

    def has_add_permission(self, request, obj=None):
        """
        Disable adding.
        """
        return True

    def has_delete_permission(self, request, obj=None):
        """
        Disable deleting.
        """
        return True

admin.site.register(Site, SiteAdmin)
