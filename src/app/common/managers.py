from urlparse import urlparse

from django.db import models
from django.db.models import *

class SiteManager(models.Manager):
    """
    Manager for the Site model.
    """
    def grouped_by_domain(self):
        """
        Returns the sites grouped by the domain.
        """
        sites = self.filter(enabled=True).values('url')
        sites = sites.order_by().annotate(count=Count('id'))
        sites = [{urlparse(site.url).netloc: torrent['count']} for site in sites]
        sites = dict((key, value) for site in sites for (key, value) in site.items())
        return sites

    def default(self, id):
        """
        Returns the default torrent site.
        """
        sites = self.filter(enabled=True)
        site = self.filter(pk=id)
        return site