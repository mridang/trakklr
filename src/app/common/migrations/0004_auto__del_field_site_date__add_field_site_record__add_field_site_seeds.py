# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Removing unique constraint on 'Site', fields ['url']
        db.delete_unique('common_site', ['url'])

        # Deleting field 'Site.date'
        db.delete_column('common_site', 'date')

        # Adding field 'Site.record'
        db.add_column('common_site', 'record', self.gf('django.db.models.fields.CharField')(default='x', max_length=512), keep_default=False)

        # Adding field 'Site.seeds'
        db.add_column('common_site', 'seeds', self.gf('django.db.models.fields.CharField')(default='', max_length=512), keep_default=False)

        # Adding field 'Site.peers'
        db.add_column('common_site', 'peers', self.gf('django.db.models.fields.CharField')(default='', max_length=512), keep_default=False)

        # Adding field 'Site.path'
        db.add_column('common_site', 'path', self.gf('django.db.models.fields.CharField')(default='', max_length=512), keep_default=False)

        # Adding field 'Site.file'
        db.add_column('common_site', 'file', self.gf('django.db.models.fields.CharField')(default='', max_length=512), keep_default=False)

        # Changing field 'Site.name'
        db.alter_column('common_site', 'name', self.gf('django.db.models.fields.CharField')(max_length=512))

        # Adding unique constraint on 'Site', fields ['name']
        db.create_unique('common_site', ['name'])


    def backwards(self, orm):
        
        # Removing unique constraint on 'Site', fields ['name']
        db.delete_unique('common_site', ['name'])

        # Adding field 'Site.date'
        db.add_column('common_site', 'date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, default=datetime.date(2012, 2, 24), blank=True), keep_default=False)

        # Deleting field 'Site.record'
        db.delete_column('common_site', 'record')

        # Deleting field 'Site.seeds'
        db.delete_column('common_site', 'seeds')

        # Deleting field 'Site.peers'
        db.delete_column('common_site', 'peers')

        # Deleting field 'Site.path'
        db.delete_column('common_site', 'path')

        # Deleting field 'Site.file'
        db.delete_column('common_site', 'file')

        # Changing field 'Site.name'
        db.alter_column('common_site', 'name', self.gf('django.db.models.fields.CharField')(max_length=2000))

        # Adding unique constraint on 'Site', fields ['url']
        db.create_unique('common_site', ['url'])


    models = {
        'common.link': {
            'Meta': {'ordering': "['-date']", 'unique_together': "(('url',),)", 'object_name': 'Link'},
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '2000'}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['common.Site']"}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '2000'})
        },
        'common.site': {
            'Meta': {'ordering': "['-id']", 'unique_together': "(('name',),)", 'object_name': 'Site'},
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'file': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'peers': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'record': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'seeds': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '2000'})
        }
    }

    complete_apps = ['common']
