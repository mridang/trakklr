# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Removing unique constraint on 'Site', fields ['name']
        db.delete_unique('common_site', ['name'])

        # Adding unique constraint on 'Site', fields ['description']
        db.create_unique('common_site', ['description'])


    def backwards(self, orm):
        
        # Removing unique constraint on 'Site', fields ['description']
        db.delete_unique('common_site', ['description'])

        # Adding unique constraint on 'Site', fields ['name']
        db.create_unique('common_site', ['name'])


    models = {
        'common.link': {
            'Meta': {'ordering': "['-date']", 'unique_together': "(('url',),)", 'object_name': 'Link'},
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '2000'}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['common.Site']"}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '2000'})
        },
        'common.site': {
            'Meta': {'ordering': "['-id']", 'unique_together': "(('description',),)", 'object_name': 'Site'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '2000'}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'file': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'peers': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'record': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'seeds': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '2000'})
        }
    }

    complete_apps = ['common']
