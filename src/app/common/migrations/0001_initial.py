# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Site'
        db.create_table('common_site', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=2000)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=2000)),
            ('date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('enabled', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('common', ['Site'])

        # Adding unique constraint on 'Site', fields ['url']
        db.create_unique('common_site', ['url'])

        # Adding model 'Link'
        db.create_table('common_link', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=2000)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=2000)),
            ('date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('site', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common.Site'])),
        ))
        db.send_create_signal('common', ['Link'])

        # Adding unique constraint on 'Link', fields ['url']
        db.create_unique('common_link', ['url'])


    def backwards(self, orm):
        
        # Removing unique constraint on 'Link', fields ['url']
        db.delete_unique('common_link', ['url'])

        # Removing unique constraint on 'Site', fields ['url']
        db.delete_unique('common_site', ['url'])

        # Deleting model 'Site'
        db.delete_table('common_site')

        # Deleting model 'Link'
        db.delete_table('common_link')


    models = {
        'common.link': {
            'Meta': {'ordering': "['-date']", 'unique_together': "(('url',),)", 'object_name': 'Link'},
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '2000'}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['common.Site']"}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '2000'})
        },
        'common.site': {
            'Meta': {'ordering': "['-date']", 'unique_together': "(('url',),)", 'object_name': 'Site'},
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '2000'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '2000'})
        }
    }

    complete_apps = ['common']
