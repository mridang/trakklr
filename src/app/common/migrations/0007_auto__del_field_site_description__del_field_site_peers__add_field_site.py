# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Removing unique constraint on 'Site', fields ['description']
        db.delete_unique('common_site', ['description'])

        # Deleting field 'Site.description'
        db.delete_column('common_site', 'description')

        # Deleting field 'Site.peers'
        db.delete_column('common_site', 'peers')

        # Adding field 'Site.title'
        db.add_column('common_site', 'title', self.gf('django.db.models.fields.CharField')(default='', max_length=2000), keep_default=False)

        # Adding field 'Site.feed'
        db.add_column('common_site', 'feed', self.gf('django.db.models.fields.CharField')(default='', max_length=512), keep_default=False)

        # Adding unique constraint on 'Site', fields ['url']
        db.create_unique('common_site', ['url'])


    def backwards(self, orm):
        
        # Removing unique constraint on 'Site', fields ['url']
        db.delete_unique('common_site', ['url'])

        # Adding field 'Site.description'
        db.add_column('common_site', 'description', self.gf('django.db.models.fields.CharField')(default='', max_length=2000), keep_default=False)

        # Adding field 'Site.peers'
        db.add_column('common_site', 'peers', self.gf('django.db.models.fields.CharField')(default='', max_length=512), keep_default=False)

        # Deleting field 'Site.title'
        db.delete_column('common_site', 'title')

        # Deleting field 'Site.feed'
        db.delete_column('common_site', 'feed')

        # Adding unique constraint on 'Site', fields ['description']
        db.create_unique('common_site', ['description'])


    models = {
        'common.link': {
            'Meta': {'ordering': "['-date']", 'unique_together': "(('url',),)", 'object_name': 'Link'},
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '2000'}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['common.Site']"}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '2000'})
        },
        'common.site': {
            'Meta': {'ordering': "['-id']", 'unique_together': "(('url',),)", 'object_name': 'Site'},
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'feed': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'file': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'record': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'seeds': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '2000'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '2000'})
        }
    }

    complete_apps = ['common']
