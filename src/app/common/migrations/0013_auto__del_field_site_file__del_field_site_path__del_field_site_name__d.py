# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Deleting field 'Site.file'
        db.delete_column('common_site', 'file')

        # Deleting field 'Site.path'
        db.delete_column('common_site', 'path')

        # Deleting field 'Site.name'
        db.delete_column('common_site', 'name')

        # Deleting field 'Site.enabled'
        db.delete_column('common_site', 'enabled')

        # Deleting field 'Site.record'
        db.delete_column('common_site', 'record')

        # Deleting field 'Site.seeds'
        db.delete_column('common_site', 'seeds')

        # Adding field 'Site.minimum'
        db.add_column('common_site', 'minimum', self.gf('django.db.models.fields.PositiveIntegerField')(default=50), keep_default=False)

        # Adding field 'Site.private'
        db.add_column('common_site', 'private', self.gf('django.db.models.fields.BooleanField')(default=False), keep_default=False)

        # Adding field 'Site.timezone'
        db.add_column('common_site', 'timezone', self.gf('django.db.models.fields.CharField')(max_length=512, null=True), keep_default=False)

        # Adding field 'Site.type'
        db.add_column('common_site', 'type', self.gf('django.db.models.fields.CharField')(max_length=5, null=True), keep_default=False)


    def backwards(self, orm):
        
        # User chose to not deal with backwards NULL issues for 'Site.file'
        raise RuntimeError("Cannot reverse this migration. 'Site.file' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'Site.path'
        raise RuntimeError("Cannot reverse this migration. 'Site.path' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'Site.name'
        raise RuntimeError("Cannot reverse this migration. 'Site.name' and its values cannot be restored.")

        # Adding field 'Site.enabled'
        db.add_column('common_site', 'enabled', self.gf('django.db.models.fields.BooleanField')(default=True), keep_default=False)

        # User chose to not deal with backwards NULL issues for 'Site.record'
        raise RuntimeError("Cannot reverse this migration. 'Site.record' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'Site.seeds'
        raise RuntimeError("Cannot reverse this migration. 'Site.seeds' and its values cannot be restored.")

        # Deleting field 'Site.minimum'
        db.delete_column('common_site', 'minimum')

        # Deleting field 'Site.private'
        db.delete_column('common_site', 'private')

        # Deleting field 'Site.timezone'
        db.delete_column('common_site', 'timezone')

        # Deleting field 'Site.type'
        db.delete_column('common_site', 'type')


    models = {
        'common.link': {
            'Meta': {'ordering': "['-date']", 'unique_together': "(('url',),)", 'object_name': 'Link'},
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'leechers': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '2000'}),
            'seeders': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['common.Site']"}),
            'torrent': ('django.db.models.fields.CharField', [], {'max_length': '2000', 'null': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '2000'})
        },
        'common.site': {
            'Meta': {'ordering': "['-id']", 'unique_together': "(('url',),)", 'object_name': 'Site'},
            'feed': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'minimum': ('django.db.models.fields.PositiveIntegerField', [], {'default': '50'}),
            'private': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'scraper': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['dynamic_scraper.Scraper']", 'null': 'True', 'blank': 'True'}),
            'scraper_runtime': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['dynamic_scraper.SchedulerRuntime']", 'null': 'True', 'blank': 'True'}),
            'timezone': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '2000'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '2000'})
        },
        'dynamic_scraper.schedulerruntime': {
            'Meta': {'object_name': 'SchedulerRuntime'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'next_action_factor': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'next_action_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'num_zero_actions': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'runtime_type': ('django.db.models.fields.CharField', [], {'default': "'P'", 'max_length': '1'})
        },
        'dynamic_scraper.scrapedobjclass': {
            'Meta': {'object_name': 'ScrapedObjClass'},
            'checker_scheduler_conf': ('django.db.models.fields.TextField', [], {'default': '\'"MIN_TIME": 1440,\\n"MAX_TIME": 10080,\\n"INITIAL_NEXT_ACTION_FACTOR": 1,\\n"ZERO_ACTIONS_FACTOR_CHANGE": 5,\\n"FACTOR_CHANGE_FACTOR": 1.3,\\n\''}),
            'comments': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'scraper_scheduler_conf': ('django.db.models.fields.TextField', [], {'default': '\'"MIN_TIME": 15,\\n"MAX_TIME": 10080,\\n"INITIAL_NEXT_ACTION_FACTOR": 10,\\n"ZERO_ACTIONS_FACTOR_CHANGE": 20,\\n"FACTOR_CHANGE_FACTOR": 1.3,\\n\''})
        },
        'dynamic_scraper.scraper': {
            'Meta': {'object_name': 'Scraper'},
            'checker_ref_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'checker_type': ('django.db.models.fields.CharField', [], {'default': "'N'", 'max_length': '1'}),
            'checker_x_path': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'checker_x_path_result': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'comments': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'content_type': ('django.db.models.fields.CharField', [], {'default': "'H'", 'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_items_read': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'max_items_save': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'pagination_append_str': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'pagination_on_start': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'pagination_page_replace': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'pagination_type': ('django.db.models.fields.CharField', [], {'default': "'N'", 'max_length': '1'}),
            'scraped_obj_class': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['dynamic_scraper.ScrapedObjClass']"}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'P'", 'max_length': '1'})
        }
    }

    complete_apps = ['common']
