# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Changing field 'Site.feed'
        db.alter_column('common_site', 'feed', self.gf('django.db.models.fields.CharField')(max_length=512, null=True))


    def backwards(self, orm):
        
        # Changing field 'Site.feed'
        db.alter_column('common_site', 'feed', self.gf('django.db.models.fields.CharField')(default='', max_length=512))


    models = {
        'common.link': {
            'Meta': {'ordering': "['-date']", 'unique_together': "(('url',),)", 'object_name': 'Link'},
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '2000'}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['common.Site']"}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '2000'})
        },
        'common.site': {
            'Meta': {'ordering': "['-id']", 'unique_together': "(('url',),)", 'object_name': 'Site'},
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'feed': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True'}),
            'file': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'record': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'seeds': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '2000'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '2000'})
        }
    }

    complete_apps = ['common']
