import sys

import requests
from datetime import *

from celery.task import *
from overseer.models import *

from app.common.models import *

class CommonChecks(PeriodicTask):
    """
    Periodic task for testing the common stuff.
    """
    run_every = timedelta(minutes=60)

    def run(self, **kwargs):
        """
        Handle.
        """
        # This part checks that movie site is working and accessible.
        try:
            print >> sys.stdout, "Checking if the movie site is working."
            response_from_site = requests.get('http://' + Site.objects.get(id=1).host)
            if response_from_site.status_code !=  requests.codes.ok: raise Exception("No response found.")

        except Exception, e:
            print >> sys.stderr, e
            message = "It seems that the site is not working."
            if Service.objects.get(slug='movie-site').event_set.count() > 0:
                if Service.objects.get(slug='movie-site').event_set.latest('id').status != 2:
                    Service.objects.get(slug='movie-site').event_set.create(status=2, description=message)

        else:
            print >> sys.stdout, "Completed successfully."
            message = "The site is working and accessible."
            if Service.objects.get(slug='movie-site').event_set.count() > 0:
                if Service.objects.get(slug='movie-site').event_set.latest('id').status == 2:
                    EventUpdate.objects.create(event=Service.objects.get(slug='movie-site').event_set.latest('id'), status=0, message=message)


        # This part checks that music site is working and accessible.
        try:
            print >> sys.stdout, "Checking if the music site is working."
            response_from_site = requests.get('http://' + Site.objects.get(id=1).host)
            if response_from_site.status_code !=  requests.codes.ok: raise Exception("No response found.")

        except Exception, e:
            print >> sys.stderr, e
            message = "It seems that the site is not working."
            if Service.objects.get(slug='music-site').event_set.count() > 0:
                if Service.objects.get(slug='music-site').event_set.latest('id').status != 2:
                    Service.objects.get(slug='music-site').event_set.create(status=2, description=message)

        else:
            print >> sys.stdout, "Completed successfully."
            message = "The site is working and accessible."
            if Service.objects.get(slug='music-site').event_set.count() > 0:
                if Service.objects.get(slug='music-site').event_set.latest('id').status == 2:
                    EventUpdate.objects.create(event=Service.objects.get(slug='music-site').event_set.latest('id'), status=0, message=message)
