from urlparse import urlparse
import json
import urllib
import socket

import bencode

from django.conf import settings
from custom.functions import *

class LocationInformation():
    """
    Tracker backend class for backend functions.
    """
    key = 'b2b3e83746ad7c156df02cab4e1e52030a4ee9df788f2c7e41bf048fda748b0e'

    @staticmethod
    def process(url) :
        """
        Gets the latitude and longitude for a given address.
        """
        try:

            host = urlparse.urlparse(url).netloc.split(':')[0]
            ip = socket.gethostbyname(host)

            urlparams = {'format': 'json', 'key': LocationInformation.key, 'ip': ip}

            location = 'http://api.ipinfodb.com/v3/ip-city/' + '?' + urllib.urlencode(urlparams)
            response = fetch_page(location)
            response = json.loads(response)

            return (response['latitude'], response['longitude'], response['countryName'])

        except Exception, e:
            raise e