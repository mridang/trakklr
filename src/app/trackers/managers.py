from datetime import *

from django.db import models
from django.db.models import *

class TrackerManager(models.Manager):
    """
    Manager for the Tracker model.
    """
    def grouped_by_type(self):
        """
        Returns the trackers grouped by the visibility type.
        """
        trackers = self.exclude(private=None).values('private')
        trackers = trackers.order_by().annotate(count=Count('id'))
        trackers = [{'private' if bool(tracker['private']) else 'public': tracker['count']}  for tracker in trackers]
        trackers = dict((key, value) for tracker in trackers for (key, value) in tracker.items())
        return trackers

    def grouped_by_date(self):
        """
        Returns the trackers grouped by the addition date.
        """
        trackers = self.extra(select={'date': 'DATE(date)'}).values('date')
        trackers = trackers.filter(date__gte=datetime.datetime(2012, 1, 1)).order_by().annotate(quantity=Count('id'))
        return trackers