import sys

import requests
import datetime

from celery.task import *
from overseer.models import *

from app.trackers.models import *

class TrackerChecks(PeriodicTask):
    """
    Periodic task for testing the tracker stuff.
    """
    run_every = timedelta(minutes=60)

    def run(self, **kwargs):
        """
        Handle.
        """
        # This part querys that new trackers have been added today.
        try:
            print >> sys.stdout, "Querying if new trackers have been added today."
            number_of_trackers = Tracker.objects.filter(date__gte=datetime.date.today() - datetime.timedelta(1)).count()
            if number_of_trackers == 0: raise Exception("No trackers found.")

        except Exception, e:
            print >> sys.stderr, e
            message = "It seems that no trackers have been added today."
            if Service.objects.get(slug='tracker-engine').event_set.latest('id').status != 2:
                Service.objects.get(slug='tracker-engine').event_set.create(status=2, description=message)

        else:
            print >> sys.stdout, "Completed successfully."
            message = "%s trackers have been added today." % number_of_trackers
            if Service.objects.get(slug='tracker-engine').event_set.latest('id').status == 2:
                EventUpdate.objects.create(event=Service.objects.get(slug='tracker-engine').event_set.latest('id'), status=0, message=message)


        # This part querys that we have queried trackers today.
        try:
            print >> sys.stdout, "Querying if the trackers have been queried today."
            number_of_querys = Query.objects.filter(date__gte=datetime.date.today() - datetime.timedelta(1)).count()
            if number_of_querys < Tracker.objects.count(): raise Exception("Not enough querys.")

        except Exception, e:
            print >> sys.stderr, e
            message = "It seems that no trackers or not enough trackers have been queried today."
            if Service.objects.get(slug='tracker-querier').event_set.latest('id').status != 2:
                Service.objects.get(slug='tracker-querier').event_set.create(status=2, description=message)

        else:
            print >> sys.stdout, "Completed successfully."
            message = "%s trackers have been queried today." % number_of_querys
            if Service.objects.get(slug='tracker-querier').event_set.latest('id').status == 2:
                EventUpdate.objects.create(event=Service.objects.get(slug='tracker-querier').event_set.latest('id'), status=0, message=message)
