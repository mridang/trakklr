from django.conf.urls.defaults import *

from app.trackers.views import *

urlpatterns = patterns('',
    url(r'^trackers/day/$', DayView.as_view(), None, 'daily_trending_trackers'),
    url(r'^trackers/week/$', WeekView.as_view(), None, 'weekly_trending_trackers'),
    url(r'^trackers/month/$', MonthView.as_view(), None, 'monthly_trending_trackers'),
    url(r'^trackers/year/$', YearView.as_view(), None, 'yearly_trending_trackers'),
    url(r'^trackers/all/$', AllView.as_view(), None, 'all_trending_trackers'),
)