from datetime import *

from django.views.generic.list import ListView
from django.views.decorators.cache import cache_page
from django.utils.decorators import method_decorator

from app.trackers.models import *

class DayView(ListView):
    """
    View to render the trending items for the day.
    """
    template_name = 'trackers/tracker.htm'
    context_object_name = 'trackers'
    
    def get_queryset(self):
        """
        Returns the list of items.
        """
        return Tracker.objects.filter(date__lte=date.today()).order_by('country')

    def get_context_data(self, **kwargs):
        """
        Returns the context data
        """
        context = super(DayView, self).get_context_data(**kwargs)
        context['method'] = 'DAY'
        return context

    @method_decorator(cache_page(60 * 24))
    def dispatch(self, *args, **kwargs):
        """
        Dispatches the request
        """
        return super(DayView, self).dispatch(*args, **kwargs)


class WeekView(ListView):
    """
    View to render the trending items for the week.
    """
    template_name = 'trackers/tracker.htm'
    context_object_name = 'trackers'
    
    def get_queryset(self):
        """
        Returns the list of items.
        """
        return Tracker.objects.filter(date__lte=date.today()).order_by('country')

    def get_context_data(self, **kwargs):
        """
        Returns the context data
        """
        context = super(WeekView, self).get_context_data(**kwargs)
        context['method'] = 'WEEK'
        return context

    @method_decorator(cache_page(60 * 24))
    def dispatch(self, *args, **kwargs):
        """
        Dispatches the request
        """
        return super(WeekView, self).dispatch(*args, **kwargs)


class MonthView(ListView):
    """
    View to render the trending items for the month.
    """
    template_name = 'trackers/tracker.htm'
    context_object_name = 'trackers'
    
    def get_queryset(self):
        """
        Returns the list of items.
        """
        return Tracker.objects.filter(date__lte=date.today()).order_by('country')

    def get_context_data(self, **kwargs):
        """
        Returns the context data
        """
        context = super(MonthView, self).get_context_data(**kwargs)
        context['method'] = 'MONTH'
        return context

    @method_decorator(cache_page(60 * 24))
    def dispatch(self, *args, **kwargs):
        """
        Dispatches the request
        """
        return super(MonthView, self).dispatch(*args, **kwargs)


class YearView(ListView):
    """
    View to render the trending items for the year.
    """
    template_name = 'trackers/tracker.htm'
    context_object_name = 'trackers'
    
    def get_queryset(self):
        """
        Returns the list of items.
        """
        return Tracker.objects.filter(date__lte=date.today()).order_by('country')

    def get_context_data(self, **kwargs):
        """
        Returns the context data
        """
        context = super(YearView, self).get_context_data(**kwargs)
        context['method'] = 'YEAR'
        return context

    @method_decorator(cache_page(60 * 24))
    def dispatch(self, *args, **kwargs):
        """
        Dispatches the request
        """
        return super(YearView, self).dispatch(*args, **kwargs)


class AllView(ListView):
    """
    View to render the trending items for all time.
    """
    template_name = 'trackers/tracker.htm'
    context_object_name = 'trackers'

    def get_queryset(self):
        """
        Returns the list of items.
        """
        return Tracker.objects.filter(date__lte=date.today()).order_by('country')

    def get_context_data(self, **kwargs):
        """
        Returns the context data
        """
        context = super(AllView, self).get_context_data(**kwargs)
        context['method'] = 'ALL'
        return context

    @method_decorator(cache_page(60 * 24))
    def dispatch(self, *args, **kwargs):
        """
        Dispatches the request
        """
        return super(AllView, self).dispatch(*args, **kwargs)