from django.contrib import admin
from django.db.models import Max

from app.trackers.models import *

class QueryInline(admin.TabularInline):
    """
    Administration interface for the checks.
    """
    list_display = ('date', 'torrent', 'latency', 'peers', 'success', 'message')
    ordering = ['-date']
    list_per_page = 100
    search_fields = ('date',)
    date_hierarchy = 'date'
    list_display_links = ['torrent']
    can_delete = False
    readonly_fields = ('date', 'torrent', 'latency', 'peers', 'success', 'message')
    exclude = None
    model = Query
    max_num=0

    def queryset(self, request):
        """
        Returns only the latest hundred queries
        """
        qs = super(QueryInline, self).queryset(request)
        return qs

    def has_add_permission(self, request, obj=None):
        """
        Disable adding.
        """
        return False

    def has_change_permission(self, request, obj=None):
        """
        Disable editing.
        """
        return False

    def has_delete_permission(self, request, obj=None):
        """
        Disable deleting.
        """
        return False


class TrackerAdmin(admin.ModelAdmin):
    """
    Administration interface for the trackers.
    """
    list_display = ('url', 'host', 'private', 'secured', 'protocol', 'country')
    ordering = ['-date']
    list_per_page = 100
    search_fields = ['url', 'secured', 'protocol', 'country']
    date_hierarchy = None
    list_display_links = ['url']
    list_filter = ('private', 'secured', 'protocol', 'country')
    readonly_fields = ('date', 'url', 'host', 'private', 'secured', 'protocol', 'country', 'torrents')
    exclude = ('latitude', 'longitude')
    inlines = [QueryInline]
    max_num = 0

    def has_add_permission(self, request, obj=None):
        """
        Disable adding.
        """
        return False

    def has_delete_permission(self, request, obj=None):
        """
        Disable deleting.
        """
        return False


admin.site.register(Tracker, TrackerAdmin)
