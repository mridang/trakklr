# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Deleting field 'Query.error'
        db.delete_column('trackers_query', 'error')

        # Adding field 'Query.seeds'
        db.add_column('trackers_query', 'seeds', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True), keep_default=False)

        # Adding field 'Query.peers'
        db.add_column('trackers_query', 'peers', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True), keep_default=False)

        # Adding field 'Query.message'
        db.add_column('trackers_query', 'message', self.gf('django.db.models.fields.CharField')(max_length=2000, null=True), keep_default=False)


    def backwards(self, orm):
        
        # Adding field 'Query.error'
        db.add_column('trackers_query', 'error', self.gf('django.db.models.fields.CharField')(max_length=2000, null=True), keep_default=False)

        # Deleting field 'Query.seeds'
        db.delete_column('trackers_query', 'seeds')

        # Deleting field 'Query.peers'
        db.delete_column('trackers_query', 'peers')

        # Deleting field 'Query.message'
        db.delete_column('trackers_query', 'message')


    models = {
        'torrents.torrent': {
            'Meta': {'ordering': "['-id']", 'unique_together': "(('hash',),)", 'object_name': 'Torrent'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'hash': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'size': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'trackers': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['trackers.Tracker']", 'symmetrical': 'False'})
        },
        'trackers.query': {
            'Meta': {'ordering': "['-date']", 'unique_together': "(('tracker', 'date'),)", 'object_name': 'Query'},
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interval': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True'}),
            'latency': ('django.db.models.fields.FloatField', [], {'null': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '2000', 'null': 'True'}),
            'peers': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'seeds': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'torrent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['torrents.Torrent']"}),
            'tracker': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trackers.Tracker']"})
        },
        'trackers.tracker': {
            'Meta': {'ordering': "['-id']", 'unique_together': "(('url', 'secured'),)", 'object_name': 'Tracker'},
            'country': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '3'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '3'}),
            'protocol': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'secured': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'torrents': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['torrents.Torrent']", 'symmetrical': 'False'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['trackers']
