# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Removing unique constraint on 'Feed', fields ['url']
        db.delete_unique('trackers_feed', ['url'])

        # Deleting model 'Feed'
        db.delete_table('trackers_feed')

        # Adding field 'Query.torrent'
        db.add_column('trackers_query', 'torrent', self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['torrents.Torrent']), keep_default=False)

        # Deleting field 'Tracker.hash'
        db.delete_column('trackers_tracker', 'hash')


    def backwards(self, orm):
        
        # Adding model 'Feed'
        db.create_table('trackers_feed', (
            ('url', self.gf('django.db.models.fields.CharField')(max_length=2000)),
            ('enabled', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('records', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=25)),
            ('frequency', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=60)),
            ('date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('trackers', ['Feed'])

        # Adding unique constraint on 'Feed', fields ['url']
        db.create_unique('trackers_feed', ['url'])

        # Deleting field 'Query.torrent'
        db.delete_column('trackers_query', 'torrent_id')

        # Adding field 'Tracker.hash'
        db.add_column('trackers_tracker', 'hash', self.gf('django.db.models.fields.CharField')(max_length=20, null=True), keep_default=False)


    models = {
        'torrents.torrent': {
            'Meta': {'ordering': "['-id']", 'unique_together': "(('hash',),)", 'object_name': 'Torrent'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'hash': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'private': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'size': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'trackers.query': {
            'Meta': {'ordering': "['-date']", 'unique_together': "(('tracker', 'date'),)", 'object_name': 'Query'},
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'error': ('django.db.models.fields.CharField', [], {'max_length': '2000', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interval': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True'}),
            'latency': ('django.db.models.fields.FloatField', [], {'null': 'True'}),
            'success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'torrent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['torrents.Torrent']"}),
            'tracker': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trackers.Tracker']"})
        },
        'trackers.tracker': {
            'Meta': {'ordering': "['-id']", 'unique_together': "(('url', 'secured'),)", 'object_name': 'Tracker'},
            'country': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '3'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '3'}),
            'protocol': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'secured': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['trackers']
