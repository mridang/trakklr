# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Feed'
        db.create_table('trackers_feed', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=2000)),
            ('date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('enabled', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('records', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=25)),
            ('frequency', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=60)),
        ))
        db.send_create_signal('trackers', ['Feed'])

        # Adding unique constraint on 'Feed', fields ['url']
        db.create_unique('trackers_feed', ['url'])

        # Adding model 'Tracker'
        db.create_table('trackers_tracker', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('secured', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('latitude', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=6, decimal_places=3)),
            ('longitude', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=6, decimal_places=3)),
            ('hash', self.gf('django.db.models.fields.CharField')(max_length=20, null=True)),
            ('protocol', self.gf('django.db.models.fields.CharField')(max_length=3)),
            ('country', self.gf('django.db.models.fields.CharField')(max_length=64, null=True)),
        ))
        db.send_create_signal('trackers', ['Tracker'])

        # Adding unique constraint on 'Tracker', fields ['url', 'secured']
        db.create_unique('trackers_tracker', ['url', 'secured'])

        # Adding model 'Query'
        db.create_table('trackers_query', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tracker', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['trackers.Tracker'])),
            ('date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('latency', self.gf('django.db.models.fields.FloatField')(null=True)),
            ('success', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('error', self.gf('django.db.models.fields.CharField')(max_length=2000, null=True)),
            ('interval', self.gf('django.db.models.fields.CharField')(max_length=128, null=True)),
        ))
        db.send_create_signal('trackers', ['Query'])

        # Adding unique constraint on 'Query', fields ['tracker', 'date']
        db.create_unique('trackers_query', ['tracker_id', 'date'])


    def backwards(self, orm):
        
        # Removing unique constraint on 'Query', fields ['tracker', 'date']
        db.delete_unique('trackers_query', ['tracker_id', 'date'])

        # Removing unique constraint on 'Tracker', fields ['url', 'secured']
        db.delete_unique('trackers_tracker', ['url', 'secured'])

        # Removing unique constraint on 'Feed', fields ['url']
        db.delete_unique('trackers_feed', ['url'])

        # Deleting model 'Feed'
        db.delete_table('trackers_feed')

        # Deleting model 'Tracker'
        db.delete_table('trackers_tracker')

        # Deleting model 'Query'
        db.delete_table('trackers_query')


    models = {
        'trackers.feed': {
            'Meta': {'ordering': "['-date']", 'unique_together': "(('url',),)", 'object_name': 'Feed'},
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'frequency': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '60'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'records': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '25'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '2000'})
        },
        'trackers.query': {
            'Meta': {'ordering': "['-date']", 'unique_together': "(('tracker', 'date'),)", 'object_name': 'Query'},
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'error': ('django.db.models.fields.CharField', [], {'max_length': '2000', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interval': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True'}),
            'latency': ('django.db.models.fields.FloatField', [], {'null': 'True'}),
            'success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'tracker': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trackers.Tracker']"})
        },
        'trackers.tracker': {
            'Meta': {'ordering': "['-id']", 'unique_together': "(('url', 'secured'),)", 'object_name': 'Tracker'},
            'country': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True'}),
            'hash': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '3'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '3'}),
            'protocol': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'secured': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['trackers']
