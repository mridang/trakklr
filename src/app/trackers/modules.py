from datetime import *

from admin_tools.dashboard import modules

from app.trackers.models import *

class TrackerStatistics(modules.DashboardModule):
    """
    Dashboard module with user registration charts.
    """
    title = "Tracker Statistics"
    template = 'trackers/dashboards/tracker_stats.html'

    def is_empty(self):
        """
        Empty Checker
        """
        return False

    def __init__(self, *args, **kwargs):
        """
        Initializer.
        """
        super(TrackerStatistics, self).__init__(*args, **kwargs)

        self.trackers = Tracker.objects.grouped_by_date()