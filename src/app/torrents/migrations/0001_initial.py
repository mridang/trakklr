# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Torrent'
        db.create_table('torrents_torrent', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('path', self.gf('django.db.models.fields.CharField')(max_length=512)),
            ('hash', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('size', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('private', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('torrents', ['Torrent'])

        # Adding unique constraint on 'Torrent', fields ['hash']
        db.create_unique('torrents_torrent', ['hash'])

        # Adding model 'Check'
        db.create_table('torrents_check', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('torrent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['torrents.Torrent'])),
            ('tracker', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['trackers.Tracker'])),
            ('date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('seeds', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True)),
            ('peers', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True)),
            ('success', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('message', self.gf('django.db.models.fields.CharField')(max_length=256, null=True)),
        ))
        db.send_create_signal('torrents', ['Check'])

        # Adding unique constraint on 'Check', fields ['tracker', 'date']
        db.create_unique('torrents_check', ['tracker_id', 'date'])


    def backwards(self, orm):
        
        # Removing unique constraint on 'Check', fields ['tracker', 'date']
        db.delete_unique('torrents_check', ['tracker_id', 'date'])

        # Removing unique constraint on 'Torrent', fields ['hash']
        db.delete_unique('torrents_torrent', ['hash'])

        # Deleting model 'Torrent'
        db.delete_table('torrents_torrent')

        # Deleting model 'Check'
        db.delete_table('torrents_check')


    models = {
        'torrents.check': {
            'Meta': {'ordering': "['-date']", 'unique_together': "(('tracker', 'date'),)", 'object_name': 'Check'},
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            'peers': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'seeds': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'torrent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['torrents.Torrent']"}),
            'tracker': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trackers.Tracker']"})
        },
        'torrents.torrent': {
            'Meta': {'ordering': "['-id']", 'unique_together': "(('hash',),)", 'object_name': 'Torrent'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'hash': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'private': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'size': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'trackers.tracker': {
            'Meta': {'ordering': "['-id']", 'unique_together': "(('url', 'secured'),)", 'object_name': 'Tracker'},
            'country': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '3'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '3'}),
            'protocol': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'secured': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['torrents']
