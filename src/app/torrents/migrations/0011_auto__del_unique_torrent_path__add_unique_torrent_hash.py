# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Removing unique constraint on 'Torrent', fields ['path']
        db.delete_unique('torrents_torrent', ['path'])

        # Adding unique constraint on 'Torrent', fields ['hash']
        db.create_unique('torrents_torrent', ['hash'])


    def backwards(self, orm):
        
        # Removing unique constraint on 'Torrent', fields ['hash']
        db.delete_unique('torrents_torrent', ['hash'])

        # Adding unique constraint on 'Torrent', fields ['path']
        db.create_unique('torrents_torrent', ['path'])


    models = {
        'torrents.check': {
            'Meta': {'ordering': "['-date']", 'unique_together': "(('tracker', 'date'),)", 'object_name': 'Check'},
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latency': ('django.db.models.fields.FloatField', [], {'null': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            'peers': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'torrent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['torrents.Torrent']"}),
            'tracker': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trackers.Tracker']"})
        },
        'torrents.torrent': {
            'Meta': {'ordering': "['-id']", 'unique_together': "(('hash',),)", 'object_name': 'Torrent'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'hash': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'scrape': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'x'", 'to': "orm['trackers.Tracker']"}),
            'size': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'trackers': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['trackers.Tracker']", 'symmetrical': 'False'})
        },
        'trackers.tracker': {
            'Meta': {'ordering': "['-id']", 'unique_together': "(('url', 'secured'),)", 'object_name': 'Tracker'},
            'announce': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'x'", 'to': "orm['torrents.Torrent']"}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '3'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '3'}),
            'protocol': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'secured': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'torrents': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['torrents.Torrent']", 'symmetrical': 'False'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['torrents']
