# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding M2M table for field trackers on 'Torrent'
        db.create_table('torrents_torrent_trackers', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('torrent', models.ForeignKey(orm['torrents.torrent'], null=False)),
            ('tracker', models.ForeignKey(orm['trackers.tracker'], null=False))
        ))
        db.create_unique('torrents_torrent_trackers', ['torrent_id', 'tracker_id'])


    def backwards(self, orm):
        
        # Removing M2M table for field trackers on 'Torrent'
        db.delete_table('torrents_torrent_trackers')


    models = {
        'torrents.check': {
            'Meta': {'ordering': "['-date']", 'unique_together': "(('tracker', 'date'),)", 'object_name': 'Check'},
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            'peers': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'seeds': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'torrent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['torrents.Torrent']"}),
            'tracker': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trackers.Tracker']"})
        },
        'torrents.torrent': {
            'Meta': {'ordering': "['-id']", 'unique_together': "(('hash',),)", 'object_name': 'Torrent'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'hash': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'private': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'size': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'trackers': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['trackers.Tracker']", 'symmetrical': 'False'})
        },
        'trackers.tracker': {
            'Meta': {'ordering': "['-id']", 'unique_together': "(('url', 'secured'),)", 'object_name': 'Tracker'},
            'country': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '3'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '3'}),
            'protocol': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'secured': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['torrents']
