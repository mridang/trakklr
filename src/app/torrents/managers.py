from datetime import *

from django.db import models
from django.db.models import *

class TorrentManager(models.Manager):
    """
    Manager for the Torrent model.
    """
    def grouped_by_type(self):
        """
        Returns the torrents grouped by the media type.
        """
        torrents = self.exclude(type='UNKNOWN').values('type')
        torrents = torrents.order_by().annotate(count=Count('id'))
        torrents = [{'movies' if torrent['type'] == 'MOVIE' else 'music': torrent['count']} for torrent in torrents]
        torrents = dict((key, value) for torrent in torrents for (key, value) in torrent.items())
        return torrents

    def grouped_by_date(self):
        """
        Returns the torrents grouped by the addition date.
        """
        torrents = self.extra(select={'date': 'DATE(date)'}).values('date')
        torrents = torrents.filter(date__gte=datetime.datetime(2012, 1, 1)).order_by().annotate(quantity=Count('id'))
        return torrents