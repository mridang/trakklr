from django.contrib import admin
from django.db.models import Max

from app.torrents.models import *

class CheckInline(admin.TabularInline):
    """
    Administration interface for the checks.
    """
    list_display = ('date', 'tracker', 'latency', 'peers', 'success', 'message')
    ordering = ['-date']
    list_per_page = 100
    search_fields = ('date',)
    date_hierarchy = 'date'
    list_display_links = ['tracker']
    can_delete = False
    readonly_fields = ('date', 'tracker', 'latency', 'peers', 'success', 'message')
    exclude = None
    model = Check
    max_num=0

    def queryset(self, request):
        """
        Returns only the latest hundred checks
        """
        qs = super(CheckInline, self).queryset(request)
        return qs

    def has_add_permission(self, request, obj=None):
        """
        Disable adding.
        """
        return False

    def has_change_permission(self, request, obj=None):
        """
        Disable editing.
        """
        return False

    def has_delete_permission(self, request, obj=None):
        """
        Disable deleting.
        """
        return False


class TorrentAdmin(admin.ModelAdmin):
    """
    Administration interface for the torrents.
    """
    list_display = ('date', 'hash', 'name', 'private', 'type', 'gigs')
    ordering = ['-date']
    list_per_page = 100
    search_fields = ['name', 'hash']
    date_hierarchy = 'date'
    list_display_links = ['hash']
    list_filter = ('private', 'date', 'type')
    readonly_fields = ('date', 'hash', 'path', 'name', 'private', 'type', 'gigs', 'trackers')
    exclude = ('size', 'files',)
    inlines = [CheckInline]
    max_num = 0

    def has_add_permission(self, request, obj=None):
        """
        Disable adding.
        """
        return False

    def has_delete_permission(self, request, obj=None):
        """
        Disable deleting.
        """
        return False


admin.site.register(Torrent, TorrentAdmin)
