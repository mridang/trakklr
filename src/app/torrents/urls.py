from django.conf.urls.defaults import *

from app.torrents.views import *

urlpatterns = patterns('',
    url(r'^torrents/day/$', DayView.as_view(), None, 'daily_trending_torrents'),
    url(r'^torrents/week/$', WeekView.as_view(), None, 'weekly_trending_torrents'),
    url(r'^torrents/month/$', MonthView.as_view(), None, 'monthly_trending_torrents'),
    url(r'^torrents/year/$', YearView.as_view(), None, 'yearly_trending_torrents'),
    url(r'^torrents/all/$', AllView.as_view(), None, 'all_trending_torrents'),
)