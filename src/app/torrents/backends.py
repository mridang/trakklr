from urlparse import urlparse
import itertools as it
import socket
import os

import bencode

from django.conf import settings
from custom.functions import *

class TorrentInformation():
    """
    Torrent backend class for backend functions.
    """
    formats = {'ISO': 'UNKNOWN', 'RAR': 'UNKNOWN', 'MPEG': 'MOVIE', 'MPG': 'MOVIE', 'MPE': 'MOVIE', 'MPA': 'MOVIE', 'MP2': 'MOVIE', 'M2A': 'MOVIE', 'MP2V': 'MOVIE', 'M2V': 'MOVIE', 'M2S': 'MOVIE', 'AVI': 'MOVIE', 'MOV': 'MOVIE', 'QT': 'MOVIE', 'ASF': 'MOVIE', 'ASX': 'MOVIE', 'WMV': 'MOVIE', 'WMA': 'MOVIE', 'WMX': 'MOVIE', 'MP4': 'MOVIE', '3GP': 'MOVIE', 'OGM': 'MOVIE', 'MKV': 'MOVIE', 'WAV': 'MUSIC', 'OGG': 'MUSIC', 'MPC': 'MUSIC', 'FLAC': 'MUSIC', 'AIFF': 'MUSIC', 'AU': 'MUSIC', 'MP3': 'MUSIC', 'AAC': 'MUSIC', 'M4A': 'MUSIC', 'WMA': 'MUSIC'}

    @staticmethod
    def process(hash) :
        """
        Gets the size, type and files from the torrent.
        """
        try:

            path = os.path.join(settings.TORRENT_DIR, hash + '.torrent')

            response = open(path, 'rb').read()
            torrent = bencode.bdecode(response)

            size = sum([file['length'] for file in torrent['info']['files']]) / 1024
            type = TorrentInformation.formats[max((sum(i.values()[0] for i in v), k) for k,v in it.groupby(sorted([{file['path'][-1][-3:].upper(): file['length']} for file in torrent['info']['files']]), key=lambda x: x.keys()[0]))[1]]
            files = torrent['info']['files']

            return (size, type, files)

        except Exception, e:
            raise e
