from datetime import *

from admin_tools.dashboard import modules

from app.torrents.models import *

class TorrentStatistics(modules.DashboardModule):
    """
    Dashboard module with user registration charts.
    """
    title = "Torrent Statistics"
    template = 'torrents/dashboards/torrent_stats.html'

    def is_empty(self):
        """
        Empty Checker
        """
        return False

    def __init__(self, *args, **kwargs):
        """
        Initializer.
        """
        super(TorrentStatistics, self).__init__(*args, **kwargs)

        self.torrents = Torrent.objects.grouped_by_date()