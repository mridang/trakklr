import sys

import requests
import datetime

from celery.task import *
from overseer.models import *

from app.torrents.models import *

class TorrentChecks(PeriodicTask):
    """
    Periodic task for testing the torrent stuff.
    """
    run_every = timedelta(minutes=60)

    def run(self, **kwargs):
        """
        Handle.
        """
        # This part checks that new torrents have been added today.
        try:
            print >> sys.stdout, "Checking if new torrents have been added today."
            number_of_torrents = Torrent.objects.filter(date__gte=datetime.date.today() - datetime.timedelta(1)).count()
            if number_of_torrents == 0: raise Exception("No torrents found.")

        except Exception, e:
            print >> sys.stderr, e
            message = "It seems that no torrents have been added today."
            if Service.objects.get(slug='torrent-engine').event_set.latest('id').status != 2:
                Service.objects.get(slug='torrent-engine').event_set.create(status=2, description=message)

        else:
            print >> sys.stdout, "Completed successfully."
            message = "%s torrents have been added today." % number_of_torrents
            if Service.objects.get(slug='torrent-engine').event_set.latest('id').status == 2:
                EventUpdate.objects.create(event=Service.objects.get(slug='torrent-engine').event_set.latest('id'), status=0, message=message)

        # This part checks that we have checked torrents today.
        try:
            print >> sys.stdout, "Checking if the torrents have been checked today."
            number_of_checks = Check.objects.filter(date__gte=datetime.date.today() - datetime.timedelta(1)).count()
            if number_of_checks < Torrent.objects.count(): raise Exception("Not enough checks.")

        except Exception, e:
            print >> sys.stderr, e
            message = "It seems that no torrents or not enough torrents have been checked today."
            if Service.objects.get(slug='torrent-checker').event_set.latest('id').status != 2:
                Service.objects.get(slug='torrent-checker').event_set.create(status=2, description=message)

        else:
            print >> sys.stdout, "Completed successfully."
            message = "%s torrents have been checked today." % number_of_checks
            if Service.objects.get(slug='torrent-checker').event_set.latest('id').status == 2:
                EventUpdate.objects.create(event=Service.objects.get(slug='torrent-checker').event_set.latest('id'), status=0, message=message)
