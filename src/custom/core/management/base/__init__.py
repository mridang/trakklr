import socket

from django.core.management.base import *
from django.core.cache import cache

import djcelery

class CustomBaseCommand(BaseCommand):

    def __init__(self, *args, **options):
        """
        Initializer
        """
        super(CustomBaseCommand, self).__init__(*args, **options)

    def handle(self, *args, **options):
        """
        Handler
        """
        djcelery.setup_loader()
        socket.setdefaulttimeout(5)
        print "Running"
        self.process(*args, **options)
        print "Finished"
        cache._cache.clear()
        cache._expire_info.clear()

    def process(self, *args, **options):
        """
        Processor
        """
        pass
