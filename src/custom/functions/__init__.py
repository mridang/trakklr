import urllib2
import urlparse
from StringIO import StringIO
import gzip
from lxml import html

from django.conf import settings

from decorators import *

@retry(3)
def fetch_page( url):
    """
    Fetches the page
    """
    request = urllib2.Request(url)
    request.add_header('Accept-encoding', 'gzip')
    request.add_header('Referer', '://'.join(urlparse.urlparse(url)[:2]) + '/')
    response = urllib2.urlopen(request)
    if response.info().get('Content-Encoding') == 'gzip':
        buf = StringIO( response.read())
        f = gzip.GzipFile(fileobj=buf)
        data = f.read()
        return data
    else:
        data = response.read()
        return data

def fetch_html_and_convert_to_lxml_document(location):
    """
    Fetches a page from the web and returns it after converting it to a document.
    """
    response = fetch_page(location)
    document = html.fromstring(response)
    document.make_links_absolute(location)
    return document