from time import sleep

from django.core.urlresolvers import reverse

def retry(howmany):
    """
    Retry decorator whcih when used on a function causes the function call to
    be retried multiple times.
    """
    def wrapper(func):
        """
        Wrapper
        """
        def function(*args, **kwargs):
            """
            Actual retyring fucntion
            """
            attempts = 0
            while attempts < howmany:
                try:
                    return func(*args, **kwargs)
                except:
                    attempts += 1
                    sleep(5)

        return function

    return wrapper