from django.conf.urls.defaults import *
from django.conf import settings

from admin.modules import *

urlpatterns = patterns('',
    url(r'^admin_tools/', include('admin_tools.urls')),
    (r'^', include('app.trackers.urls')),
    (r'^peavy/', include('peavy.urls', namespace='peavy')),
    (r'^', include('app.common.urls')),
    (r'^admin/', include(admin.site.urls)),
    (r'^', include('app.torrents.urls')),
    (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}, 'static_directory'),
)
